from bs4 import BeautifulSoup
from prettytable import PrettyTable
import requests

BASE_LINK = 'https://thuisbezorgd.nl'
POSTAL_CODE = '2564'
CATEGORY = 'Poké bowl'
MAIN_LINK = f'{BASE_LINK}/eten-bestellen-{POSTAL_CODE}'


def get_main_page():
    '''Gets the main page of the website'''
    print('- GET - main page')
    return BeautifulSoup(requests.get(MAIN_LINK).content, 'html.parser')


def get_category_entries(main_page):
    '''Gets all the restaurants that match the category'''
    print('- PROC - matching by category')
    restaurants = main_page.find_all(class_='restaurant')
    category_entries = []

    for restaurant in restaurants:
        kitchens = restaurant.find('div', class_='kitchens')

        if kitchens is not None:
            if CATEGORY in kitchens.text:
                info = restaurant.find(class_="restaurantname").a
                name = info.text.lstrip().rstrip()
                link = info.attrs['href']

                category_entries.append((name, link))

    return category_entries


def get_category_pages(category_entries):
    '''Gets all the pages of the restaurants'''
    pages = []

    for entry in category_entries:
        link = f'{BASE_LINK}{entry[1]}'
        print(f'- GET - {link}')
        entry_soup = BeautifulSoup(requests.get(link).content, 'html.parser')
        if entry_soup is not None:
            pages.append(entry + (entry_soup,))

    return pages


def get_page_average(pages):
    '''Calculates the average prices of a restaurant'''
    print('- PROC - calculating averages')
    restaurant_info = []
    for page in pages:
        total = 0
        count = 0
        average = 0
        meal_prices = page[2].find_all(class_='meal__price')

        for price in meal_prices:
            count += 1
            total += float(price.text.lstrip().rstrip().replace(',', '.')[2::])

        average = round(total / count, 2)
        restaurant_info.append((page[0], average))

    return restaurant_info


def display_top_restaurants(restaurants, top=5):
    '''Displays the top cheapest restaurants'''
    print(f'\n- DONE - displaying top {top} cheapest restaurants on average')
    table = PrettyTable(['Name', 'Price in €'])
    restaurants = sorted(restaurants, key=lambda tup: tup[1])

    for i in range(top):
        table.add_row(restaurants[i])

    print(table)


def main():
    print(f'- PARAMS - ({CATEGORY}, {POSTAL_CODE})')
    main_page = get_main_page()
    restaurants = get_page_average(
        get_category_pages(get_category_entries(main_page)))
    display_top_restaurants(restaurants)


if __name__ == '__main__':
    main()
